package tickTackToe;

import java.util.ArrayList;

public class Board {
    private Cell cells[][] = new Cell[3][3];


    public void placeMark(int a, int b, int state) {
        cells[a][b].setState(state);
    }


    public boolean hasWon() {
        int countX = 0;
        int countO = 0;
        int countRowO = 0;
        int countRowX = 0;
        int leftDiagX = 0;
        int leftDiagO = 0;
        int rightDiagX = 0;
        int rightDiagO = 0;
        for (int x = 0; x < cells.length; ++x) {
            for (int y = 0; y < cells[x].length; ++y) {
                if (cells[x][y].getState() == 0) {
                    ++countO;
                }
                if (cells[x][y].getState() == 1) {
                    ++countX;
                }
                if (cells[y][x].getState() == 0) {
                    ++countRowO;
                }
                if (cells[y][x].getState() == 1) {
                    ++countRowX;
                }
                if (cells[2-x][x].getState() == 1) {
                    ++rightDiagX;
                }
                if (cells[2-x][x].getState() == 0) {
                    ++rightDiagO;
                }
                if (cells[x][2-x].getState() == 1) {
                    ++leftDiagX;
                }
                if (cells[x][2-x].getState() == 0) {
                    ++leftDiagO;
                }
            }

            if (countO == 3 || countX == 3) {
                return true;
            } else if (countRowO == 3 || countRowX == 3) {
                return true;
            } else if (rightDiagX == 3 || rightDiagO == 3) {
                return true;
            }
        }
        return false;
    }
}

