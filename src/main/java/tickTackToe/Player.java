package tickTackToe;

public class Player {
    private String name;
    private String role;
    private Board board;


    public Player(String name, String role, Board board) {
        this.name = name;
        this.role = role;
        this.board = board;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public Board getBoard() {
        return board;
    }

}
