package tickTackToe;

import tickTackToe.Board;
import tickTackToe.Player;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        System.out.println("Welcome to game Tick Tack Toe ! ");
        System.out.println("--------------------------------");
        System.out.println("Player 1 - your role is O. Player 2 - your role is X.");
        System.out.println("--------------------------------");
        System.out.println("WARNING: Write only number, where 0 equal O and 1 equal X !!!");
        System.out.println("FOR EXAMPLE: 27 - will be filled X in cell 7 ans so on... ");
        System.out.println("--------------------------------");
        Object[][] field = new Object[3][5];
        field[0][0] = "[0]";
        field[0][1] = "|";
        field[0][2] = "[1]";
        field[0][3] = "|";
        field[0][4] = "[2]";
        field[1][0] = "[3]";
        field[1][1] = "|";
        field[1][2] = "[4]";
        field[1][3] = "|";
        field[1][4] = "[5]";
        field[2][0] = "[6]";
        field[2][1] = "|";
        field[2][2] = "[7]";
        field[2][3] = "|";
        field[2][4] = "[8]";
        for (int v = 0; v < field.length; v++) {
            for (int k = 0; k < field[v].length; k++) {
                System.out.print(field[v][k] + "\t");
            }
            System.out.println();
        }
        System.out.println(field);
/*===========================================================================*/
        Board board = new Board();
        Player player1 = new Player("P1", "O", board);
        Player player2 = new Player("P2", "X", board);

//        player1.getBoard().placeMark(1,1, 0);
//        player2.getBoard().placeMark(2,0, 1);
        Scanner in = new Scanner(System.in);

        while (!board.hasWon() == false) {
            int s = in.nextInt();
            int p = (int) (Math.random() * 2);
            try {
                if (!(s > -1 && s < 8)) {
                    System.out.println("Invalid input number; re-enter slot number:");
                    continue;
                }

            } catch (InputMismatchException e) {
                System.out.println("Invalid input number; re-enter slot number:");
                continue;
            }
            if (p == 1) {
                System.out.println("Player 1 - enter slot number:");
                switch (s) {
                    case 0:
//            if (s == 0){
                        player1.getBoard().placeMark(0, 0, 0);
                        if (board.hasWon() == true) {
                            System.out.println("Player 1 - has won!");
                            break;
                        } else continue;
//            }else
                    case 1:
//                if(s == 1){
                        player1.getBoard().placeMark(0, 1, 0);
                        if (board.hasWon() == true) {
                            System.out.println("Player 1 - has won!");
                            break;
                        } else continue;
//                }else
                    case 2:
//                    if(s == 2) {
                        player1.getBoard().placeMark(0, 2, 0);
                        if (board.hasWon() == true) {
                            System.out.println("Player 1 - has won!");
                            break;
                        } else continue;
//                    }else
                    case 3:
//                    if(s == 3) {
                        player1.getBoard().placeMark(1, 0, 0);
                        if (board.hasWon() == true) {
                            System.out.println("Player 1 - has won!");
                            break;
                        } else continue;
//                    }else
                    case 4:
//                    if(s == 4) {
                        player1.getBoard().placeMark(1, 1, 0);
                        if (board.hasWon() == true) {
                            System.out.println("Player 1 - has won!");
                            break;
                        } else continue;
//                    }else
                    case 5:
//                    if(s == 5) {
                        player1.getBoard().placeMark(1, 2, 0);
                        if (board.hasWon() == true) {
                            System.out.println("Player 1 - has won!");
                            break;
                        } else continue;
//                    }else
                    case 6:
//                    if(s == 6) {
                        player1.getBoard().placeMark(2, 0, 0);
                        if (board.hasWon() == true) {
                            System.out.println("Player 1 - has won!");
                            break;
                        } else continue;
//                    }else
                    case 7:
//                    if(s == 7) {
                        player1.getBoard().placeMark(2, 1, 0);
                        if (board.hasWon() == true) {
                            System.out.println("Player 1 - has won!");
                            break;
                        } else continue;
//                    }else
                    case 8:
//                    if(s == 8) {
                        player1.getBoard().placeMark(2, 2, 0);
                        if (board.hasWon() == true) {
                            System.out.println("Player 1 - has won!");
                            break;
                        } else continue;
//                    }
                }
            }
///*===================================================================================================================*/
            if (p == 2) {
                System.out.println("Player 2 - enter slot number:");
                switch (s) {
                    case 0:
//            if (s == 0){
                player2.getBoard().placeMark(0,0, 1);
                if (board.hasWon() == true){
                    System.out.println("Player 2 - has won!");
                    break;
                }else continue;
//            }else
                    case 1:
//            if(s == 1){
                player2.getBoard().placeMark(0,1, 1);
                if (board.hasWon() == true){
                    System.out.println("Player 2 - has won!");
                    break;
                }else continue;
//            }else
                    case 2:
//            if(s == 2) {
                player2.getBoard().placeMark(0, 2, 1);
                if (board.hasWon() == true){
                    System.out.println("Player 2 - has won!");
                    break;
                }else continue;
//            }else
                    case 3:
//            if(s == 3) {
                player2.getBoard().placeMark(1, 0, 1);
                if (board.hasWon() == true){
                    System.out.println("Player 2 - has won!");
                    break;
                }else continue;
//            }else
                    case 4:
//            if(s == 4) {
                player2.getBoard().placeMark(1, 1, 1);
                if (board.hasWon() == true) {
                    System.out.println("Player 2 - has won!");
                    break;
                }else continue;
//            }else
                    case 5:
//            if(s == 5) {
                player2.getBoard().placeMark(1, 2, 1);
                if (board.hasWon() == true) {
                    System.out.println("Player 2 - has won!");
                    break;
                }else continue;
//            }else
                    case 6:
//            if(s == 6) {
                player2.getBoard().placeMark(2, 0, 1);
                if (board.hasWon() == true) {
                    System.out.println("Player 2 - has won!");
                    break;
                }else continue;
//            }else
                    case 7:
//            if(s == 7) {
                player2.getBoard().placeMark(2, 1, 1);
                if (board.hasWon() == true) {
                    System.out.println("Player 2 - has won!");
                    break;
                }else continue;
//            }else
                    case 8:
//            if(s == 8) {
                player2.getBoard().placeMark(2, 2, 1);
                if (board.hasWon() == true) {
                    System.out.println("Player 2 - has won!");
                    break;
                }else continue;
//            }
                }
            }
        }
    }
}


